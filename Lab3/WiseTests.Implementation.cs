using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TestProject.Common.Enums;
using TestProject.SDK;
using TestProject.SDK.Tests;
using TestProject.SDK.Tests.Helpers;

namespace UnitTests
{
    public class WiseTests_Implementation 
    { 
        
        public class F1ConsentToCookiesTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            accept.Click();

            return ExecutionResult.Passed;
        }
    }
    public class F2ChangeLanguageTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            var open = driver.FindElement(
                By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[1]/li[4]/button"));
            open.Click();
            var language =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[1]/li[4]/ul/li[4]/button"));
            language.Click();
            var text = driver.FindElement(
                By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[1]/li[1]/button/span[1]"));

            if (text.Text.Contains("Money transfer"))
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    
    public class F3GetHelpTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            var helpLink =
                driver.FindElement(By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[1]/li[3]/a"));
            helpLink.Click();
            Thread.Sleep(1000);
            var windowHandle = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle);
            var currentUrl = driver.Url;
            if (currentUrl == "https://wise.com/help/")
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    
    public class F8CompareTest : IWebTest
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            IWebElement input = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/input")));
            Thread.Sleep(1000);
            var toConvertText =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/ul/li[2]/span[2]"));
            string exampleTrimmed = toConvertText.Text.TrimEnd(' ', 'E', 'U', 'R');
            double toConvert = Convert.ToDouble(exampleTrimmed);
            var rateText = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/ul/li[3]/span[2]/a/span[1]"));
            double rate = Convert.ToDouble(rateText.Text);
            var expected = Math.Round(rate * toConvert, 2);
            Thread.Sleep(1000);
            var result =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[2]/div/input"));
            if (result.GetAttribute("value").Contains(expected.ToString("N")))
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    
    public class F9ChangeInputTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            IWebElement input = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/input")));
            input.Click();
            Thread.Sleep(1000);
            while (!input.GetAttribute("value").Equals(""))
            {
                input.SendKeys(Keys.Backspace);
            }

            input.SendKeys("100");
            var button = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/button"));
            button.Click();
            Thread.Sleep(500);
            var currency = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/ul/li[4]"));
            currency.Click();
            Thread.Sleep(1000);
            var currencySelected = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/button/span[1]"));
            Assert.AreEqual("GBP", currencySelected.Text);
            var toConvertText =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/ul/li[2]/span[2]"));
            string exampleTrimmed = toConvertText.Text.TrimEnd(' ', 'G', 'B', 'P');
            double toConvert = Convert.ToDouble(exampleTrimmed);
            var rateText = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/ul/li[3]/span[2]/a/span[1]"));
            double rate = Convert.ToDouble(rateText.Text);
            var expected = Math.Round(rate * toConvert, 2);
            Thread.Sleep(1000);
            var result =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[2]/div/input"));
            if (result.GetAttribute("value").Contains(expected.ToString("N")))
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    
    public class F10ChangeCurrencyTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
                            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            IWebElement input = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/input")));
            Thread.Sleep(1000);

            var button = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/button"));
            button.Click();
            Thread.Sleep(500);
            var currency = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/ul/li[4]"));
            currency.Click();
            Thread.Sleep(1000);
            var currencySelected = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/button/span[1]"));
            Assert.AreEqual("GBP", currencySelected.Text);
            var toConvertText =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/ul/li[2]/span[2]"));
            string exampleTrimmed = toConvertText.Text.TrimEnd(' ', 'G', 'B', 'P');
            double toConvert = Convert.ToDouble(exampleTrimmed);
            var rateText = driver.FindElement(By.XPath(
                "/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/ul/li[3]/span[2]/a/span[1]"));
            double rate = Convert.ToDouble(rateText.Text);
            var expected = Math.Round(rate * toConvert, 2);
            Thread.Sleep(1000);
            var result =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/main/div[1]/div/div/div[2]/div[1]/div[1]/div[2]/div/input"));
                if (result.GetAttribute("value").Contains(expected.ToString("N")))
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    
    public class F4BusinessCustomerTest : IWebTest
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            var business = driver.FindElement(By.XPath("/html/body/div[1]/div/header/div/nav/div/ul/li[3]/a"));
            business.Click();
            Thread.Sleep(1000);
            var windowHandle = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle);
            var currentUrl = driver.Url;
            if (currentUrl == "https://wise.com/gb/business/")
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }

    public class F5RegisterTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper) 
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            var register =
                driver.FindElement(By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[2]/li[2]/a"));
            register.Click();
            Thread.Sleep(1000);
            var windowHandle = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle);
            new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(By.XPath(
                    "/html/body/div[1]/div/div/div/section/div/div/div/div/div/div/form/div[3]/div/input")));
            new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/div/div/section/div/div/div/div/div/div/form/button")));

            return ExecutionResult.Passed;
        }
    }
    
    public class F6PrivacyTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            var register =
                driver.FindElement(By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[2]/li[2]/a"));
            register.Click();
            Thread.Sleep(1000);
            var windowHandle = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle);
            Thread.Sleep(1000);
            var policy =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/div/div/section/div/div/div/div/div/div/p[1]/a[2]"));
            policy.Click();
            Thread.Sleep(1000);
            var windowHandle1 = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle1);
            var currentUrl = driver.Url;
            if (currentUrl == "https://wise.com/gb/legal/privacy-policy")
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    public class F7LogInTest : IWebTest
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper) 
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            //var accept = driver.FindElement(By.XPath("/html/body/div[2]/div/div/article/div[3]/button[1]"));
            //accept.Click();
            var logIn = driver.FindElement(
                By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[2]/li[1]/a"));
            logIn.Click();
            Thread.Sleep(1000);
            var windowHandle = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle);
            Thread.Sleep(1000);
            IWebElement greeting = driver.FindElement(By.XPath("/html/body/div[1]/div/div/div[2]/div[2]/h1"));
            Assert.IsTrue(greeting.Text.Contains("Welcome back."));
            new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/div/div[2]/div[2]/div[2]/form/div[1]/div/input")));
            new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/div/div[2]/div[2]/div[2]/form/div[3]/div/input")));
            new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(
                ExpectedConditions.ElementToBeClickable(
                    By.XPath("/html/body/div[1]/div/div/div[2]/div[2]/div[2]/form/div[4]/button")));
            return ExecutionResult.Passed;
        }
    }
    
    public class NF1PageLoadTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var ResponseTime = Convert.ToInt32(js.ExecuteScript("return window.performance.timing.domContentLoadedEventEnd-window.performance.timing.navigationStart;"));
            
            
            Console.WriteLine(string.Format("Page {0} loading time is {1} ms", driver.Title, ResponseTime));
            
            if (ResponseTime < 1500)
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }
    public class NF2DNSLookUpTest : IWebTest
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var DNSResponseTime = Convert.ToInt32(js.ExecuteScript("return window.performance.timing.domainLookupEnd-window.performance.timing.domainLookupStart;"));

            Console.WriteLine(string.Format("DNS lookup {0} time is {1} ms", driver.Title, DNSResponseTime));
            
            if (DNSResponseTime < 1)
            {
                return ExecutionResult.Passed;
            }
            return ExecutionResult.Failed;
        }
    }

    public class NF3PrivacyPageLoadTest : IWebTest 
    {
        ExecutionResult IWebTest.Execute(WebTestHelper helper)
        {
            var driver = helper.Driver;
            driver.Url = "https://wise.com";
            driver.Navigate().GoToUrl(driver.Url + "/");
            var register =
                driver.FindElement(By.XPath("/html/body/div[1]/div/header/div/nav/div/div[2]/ul[2]/li[2]/a"));
            register.Click();
            Thread.Sleep(1000);
            var windowHandle = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle);
            Thread.Sleep(1000);
            var policy =
                driver.FindElement(
                    By.XPath("/html/body/div[1]/div/div/div/section/div/div/div/div/div/div/p[1]/a[2]"));
            policy.Click();
            Thread.Sleep(1000);
            var windowHandle1 = driver.WindowHandles.LastOrDefault();
            driver.SwitchTo().Window(windowHandle1);
            IJavaScriptExecutor js = (IJavaScriptExecutor) driver;
            var ResponseTime = Convert.ToInt32(js.ExecuteScript(
                "return window.performance.timing.domContentLoadedEventEnd-window.performance.timing.navigationStart;"));

            Console.WriteLine($"Page {driver.Title} loading time is {ResponseTime} ms");

            if (ResponseTime < 1500)
            {
                return ExecutionResult.Passed;
            }

            return ExecutionResult.Failed;
        }
    }

    public class NF4RequestTest : IWebTest 
        {
            ExecutionResult IWebTest.Execute(WebTestHelper helper)
            {
                var driver = helper.Driver;
                driver.Url = "https://wise.com";
                driver.Navigate().GoToUrl(driver.Url + "/");
               
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                var requestTime = Convert.ToInt32(js.ExecuteScript("return window.performance.timing.responseStart-window.performance.timing.requestStart;"));
            
                Console.WriteLine($"Page {driver.Title} request time is {requestTime} ms");
            
                if (requestTime < 400)
                {
                    return ExecutionResult.Passed;
                }
                return ExecutionResult.Failed;
            }
            }
        }
    }
    