using NUnit.Framework;
using TestProject.Common.Enums;
using TestProject.SDK;

namespace UnitTests

{
    

    public class WiseTests
        {
            Runner runner;
            private string DevToken = "Iw72CFLmkodlOo6Q8JF2lZWq8wqZr7Q2K2RqjltY4EA1"; 

            [OneTimeSetUp]
            public void SetUp() 
            {
                runner = new RunnerBuilder(DevToken).AsWeb(AutomatedBrowserType.Chrome).Build();
            }

            [NUnit.Framework.Test, Order(1)] 
            public void ConsentToCookiesTests() 
            {
                runner.Run(new WiseTests_Implementation.F1ConsentToCookiesTest());
            }

            [NUnit.Framework.Test, Order(2)] 
            public void ChangeLanguageTests()
            {
                runner.Run(new WiseTests_Implementation.F2ChangeLanguageTest());
            }

            [NUnit.Framework.Test, Order(3)] 
            public void GetHelpTests()
            {
                runner.Run(new WiseTests_Implementation.F3GetHelpTest());
            }

            [NUnit.Framework.Test, Order(8)] 
            public void CompareTests()
            {
                runner.Run(new WiseTests_Implementation.F8CompareTest());
            }

            [NUnit.Framework.Test, Order(10)] 
            public void ChangeCurrencyTests()
            {
                runner.Run(new WiseTests_Implementation.F10ChangeCurrencyTest());
            }

            [NUnit.Framework.Test, Order(9)] 
            public void ChangeInputs()
            {
                runner.Run(new WiseTests_Implementation.F9ChangeInputTest());
            }

            [NUnit.Framework.Test, Order(4)] 
            public void BusinessCustomerTests()
            {
                runner.Run(new WiseTests_Implementation.F4BusinessCustomerTest());
            }

            [NUnit.Framework.Test, Order(5)]
            public void RegisterTests()
            {
                runner.Run(new WiseTests_Implementation.F5RegisterTest());
            }

            [NUnit.Framework.Test, Order(6)] 
            public void PrivacyTests()
            {
                runner.Run(new WiseTests_Implementation.F6PrivacyTest());
            }

            [NUnit.Framework.Test, Order(7)] 
            public void LogInTests()
            {
                runner.Run(new WiseTests_Implementation.F7LogInTest());
            }
            
            [NUnit.Framework.Test, Order(11)] 
            public void PageLoadTests() 
            {
                runner.Run(new WiseTests_Implementation.NF1PageLoadTest()); 
            }

            [NUnit.Framework.Test, Order(12)] 
            public void DNSLookUpTests() 
            {
                runner.Run(new WiseTests_Implementation.NF2DNSLookUpTest()); 
            }
            
            [NUnit.Framework.Test, Order(13)] 
            public void PrivacyPageLoadTests() 
            { 
                runner.Run(new WiseTests_Implementation.NF3PrivacyPageLoadTest()); 
            }
            [NUnit.Framework.Test, Order(14)] 
            public void BusinessCustomerRedirectTests() 
            {
                runner.Run(new WiseTests_Implementation.NF4RequestTest());
            }


            [OneTimeTearDown]
            public void TearDown() 
            {
                runner.Dispose();
            }
        }
    }